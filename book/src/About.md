# Dracon

<!-- Plugin description -->
**Dracon** is an unofficial integration plugin for Pijul and IntelliJ Platform.
<!-- Plugin description end -->

# Roadmap

## March (0.1.x)
- [X] Integrate Pijul Init and Pijul Add with IntelliJ 
- [X] Integrate Pijul with existing DVCS mechanism.
- [X] Support file status for visual feedback.
- [X] Support loading file changes
  - Supported, but does introduce overheads. Read **[file changes loading](#file-changes-loading-overhead)** section.
- [X] Implement Pijul Record through GUI.
  - [ ] Support amend.
  - ~~Record text file opens in default editor instead of IntelliJ Editor.~~
  - [X] Record text is opened in IntelliJ IDEA powered by [editor-server](https://crates.io/crates/editor-server) backend.
- [X] File history support
  - [X] Allows users to compare between revisions
  - [X] Allows users to see affected files for any given revision.

## May (0.2.x)
- [ ] Pijul Basic Log View
- [ ] Channel switch
- [ ] Push, pull and remote stream configuration.
- [ ] ...

## How to install

Dracon is in a heavily experimental state, and it depends on IntelliJ 212.5284.40.

### Pick a build

The easiest way is to pick an officially packaged version of Dracon which we periodically publish. 

First download an **experimental** or **nightly** build from [builds](https://nest.pijul.com/Jonathan/Dracon:main/MJDBCNFHSHAR4.BEAAA) directory (probably you will need to clone this repo to get the zip file), then go to IntelliJ IDEA Plugins section and click in the gear on the right side of *Installed tab* and select *Install plugin from Disk...* and select the downloaded Dracon *.zip*. Now you're ready to go.

Note, Dracon experimental builds are for testing purposes only, nightly builds are more tested against fresh installs and others VCS plugins integration, we do not take responsibility for any damage caused to your files and repository corruption.


### Build on your own

First clone Dracon repository:

```bash
pijul clone https://nest.pijul.com/Jonathan/Dracon
```

Then create build and package the plugin using `buildPlugin` gradle task:

For Unix:
```bash
./gradlew buildPlugin
```

For Windows:
```batch
.\gradlew.bat buildPlugin
```

The generated plugin file could be find in `build/distributions` path, the installation of the plugin `.zip` is exactly the same as for an officially made build: Go to IntelliJ IDEA Plugins section and click in the gear on the right side of *Installed* tab and select *Install plugin from Disk...* and select the generated Dracon *.zip*.

### JetBrains Marketplace

Dracon will be made available through official JetBrains Plugin Repository once it became stable.

### Dependencies
Recently versions of Dracon requires [editor-server](https://crates.io/crates/editor-server) to be installed in order to work, ~~the final
release version will come bundled with the [editor-server](https://crates.io/crates/editor-server).~~ The most recent version of Dracon does prompts for Pijul and Editor-Server download, as this requires Cargo in order to install dependencies if it is not installed, Dracon will prompt for Rustup and Cargo installation, it does the baby steps for installation, but you take the lead as soon as choices need to be made.

## File Changes Loading Overhead
Pijul does not have an easy way to retrieve the state of a file in a given revision, in order to support this, Dracon creates a copy of the repository in a temporary directory and execute `pijul unrecord` against all revisions that happened after the given revision.

For CoW based file systems, such as ZFS and Btrfs, this operation is very cheap, and as long as all the files are not modified when **pijul unrecord** changes, this overhead may not be perceptible. However, there are some cases when this overhead becomes very perceptible, for example, when the amount of revisions between the target revision (the revision who you want to see), and the current revision (the last recorded revision), are bigger. 

So, how much bigger is bigger enough to be perceptible? I'm not sure, I don't have enough samples to conclude the exact amount of changes that are needed in order to this operation overhead be perceptible, it also depends on the complexity of the entire operation, how many files changed between these revisions, the complexity of these changes, the performance of the secondary storage (Read/Write). I've been tested Dracon and made a bunch of performance improvements, such as caching, which improved the overall performance of revision loading and project loading.

~~An NVMe SSD and a CoW file system alleviate this a bit, however I'm always working in strategies to improve the performance, however when Pijul provides a way to see the state of a file in a given revision, I will use it instead of the actual routine.~~

In the most current versions, Dracon does a bunch of caching and preloads file revisions, also caching code has been the focus of the development now that Dracon is almost ready for a nightly release. However, there is room for improvements.

~~Also, I'm currently improving the rollback algorithm to *unrecord* only revision which actually change the file, instead of all revisions between the actual revision (inclusively) and the target revision (exclusively) by looking at `pijul credit`~~

Plans have changed, instead of only un-recording changes that `pijul credit` annotated, Dracon save the state of other files of a given revision, speeding things a bit and reducing the amount of `pijul unrecord` we execute (currently only for the caching).

## Known issues

### Dracon log history does not show related records

Dracon shows history like:

![](pijul-no-log-tree.png)

Instead of something link:

![](pijul-log-tree-would-be.png)

This is because the relation of Pijul patches are not linear, because of this, the following happens when
we try to correctly build the relation between records:

![](pijul-log-tree.png)

### Dracon does not track added files in commit window and in VCS History

Dracon still does track changed files, so doing a `pijul record -a` when adding files fixes this.
  
### Dracon does not show `Create Pijul repository` option when fresh installed.

Restarting IDEA does fix this.

### A bunch of other minor issues

# FAQ

## Why Dracon and not Pijul for IDEA (or something like that)?

Because Dracon is not an official plugin, and I want to make sure that it is clear to anyone who uses this plugin. Dracon is an individual project being developed by [Jonathan H. R. Lopes](https://github.com/JonathanxD) which does not have any relation with Pijul creator. Also, Dracon uses MIT license, allowing to anyone, and that includes the Pijul creator, to contribute and create your own version of Dracon, if wish to.

## Why Dracon is developed against IntelliJ EAP instead of a stable releases?

Dracon is being developed based in actual [HG4Idea](https://github.com/JetBrains/intellij-community/tree/master/plugins/hg4idea) and [Git4Idea](https://github.com/JetBrains/intellij-community/tree/master/plugins/git4idea) plugins, as well based in current code of [IntelliJ IDEA Community](https://github.com/JetBrains/intellij-community/), and is planned to be released by the end of first semester of 2021, when release time comes, the IntelliJ EAP will be already released as stable, also I don't want to support older releases of IntelliJ IDEA because this involves basing on deprecated code, and this is my first IDEA plugin, so the more I focus the present than the past, more I deliver in terms of better and stable features.

## Could I contribute to Dracon development?

I'm currently focused on learning how IDEA Plugin platform works and how DVCS is implemented in IDEA, so until Dracon is officially released, I will not be accepting contributions, but Dracon is licensed under MIT, you are free to create your own version of Dracon and developing it on your own.

## When Dracon will be official released?

I will be periodically releasing Dragon experimental builds, but Dracon stable release is planned by the end of first semester of 2021.
